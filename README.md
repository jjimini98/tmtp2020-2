# 2020-2 TextMining Project
__Text mining team project in Department of Bigdata engineering__
________
## 1. 프로젝트의 목적
호텔리뷰를 작성할 때, 내가 작성한 리뷰에 상응하는 적절한 별점을 매기기 싶어 본 프로젝트를 진행하였다.     
즉, 다른 사람들의 리뷰데이터를 사용해 **내 리뷰에 맞는 별점을 예측**해주는 모델이다. 

## 2. 사용한 모델 
- Bidirectional LSTM 
- GRU 

## 3. 설치 방법
GRU모델과 LSTM모델의 저장 경로가 달라 서로 다른 방법으로 설치하여야한다.

- GRU모델 저장위치
```python
PATH="/home/u1015/tmtp2020-2/hotel_reveiw/model/GRU2.model"
```
- LSTM모델 저장위치

```python
PATH = "tmtp2020-2/train_LSTM/model_dir/bidirectional_LSTMmodel"
```

## 3. 의존성
- python 3.x


## 4. 사용(실행) 방법

1. 아래의 코드에 따라 모델을 로드하고 프로젝트에서 파이썬 파일을 기져온다.
2. 파이썬 파일을 우선 실행 후 모델을 실행시키고 예측하고자 하는 문장을 텍스트 변수에 넣는다.
3. 변수 n는 자신이 원하는 예측값의 갯수를 지정한다.

```python
model = load_model(path)

text = [] 
n = 0

y_pred = model.predict(text, batch_size = 50)
x_test_idx= np.random.choice(text.hape[0], n)

for i in range(n):
    print(text[x_test_idx[i]])
    print('True : ' + str(np.argmax(y_test[x_test_idx[i]])) + ', Predict : ' + str(np.argmax(y_pred[i])))
```
## 5. 라이센스 
연구나 비영리적인 목적으로 본 프로젝트 사용이 가능하다.
추가적인 내용은 아래의 이메일로 연락

유지민 [jjimini98@naver.com](url)

이지윤 [junepass6@gmail.com](url)
