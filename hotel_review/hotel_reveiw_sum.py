import pandas as pd
import nltk
import matplotlib.pyplot as plt
from nltk import word_tokenize
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
import tensorflow as tf
import numpy as np

data = pd.read_csv("hotel.csv")

#긍정과 부정 리뷰를 띄어쓰기로 합친 새로운 열 total_review 생성
data['total_review'] = data['Negative_Review']+" "+data['Positive_Review']

#필요없는 열 모두 제거
del data['Unnamed: 0']
del data['Negative_Review']
del data['Positive_Review']

# No negative, No positive, Nothing 은 빈칸으로 replace
for x in data['total_review']:
    if 'No Negative' in x:
       data['total_review']= data['total_review'].str.replace("No Negative","-")

    if 'No Positive' in x:
        data['total_review']= data['total_review'].str.replace("No Positive","-")

    if 'Nothing' in x:
        data['total_review'] = data['total_review'].str.replace('Nothing',"-")

# 열 순서 변경
data = data[['total_review','Reviewer_Score']]

# 데이터의 형태를 확인하려고 만들어둔겁니다. 나중에 삭제 가능
# data.to_csv('edit_hotel.csv',mode = 'a')


# one hot encoding ->하던 안하던 결과가 같다. 모델 훈련시킬 때 변수이름은 score로 사용하면 됩니다
score = pd.DataFrame( data['Reviewer_Score'] )
for t in score:
    if t == "Reviewer_Score":
        continue
    else:
        t = int(t)
        score = to_categorical(t)
# print(score)

 # 문장분리
comment = [] #1031477
for c in data['total_review']:
        comment.append(c)

#토큰화
token = [] #token length : 1031477 리스트 안에 리스트형태
for i in range(0, len(comment)):
    text = word_tokenize(comment[i])
    token.append(text)


#불용어 제거
vocab = {}
remove_one_two = [e for e in token if len(e)>=3] #1014326
stopwords = nltk.corpus.stopwords.words('english')
words = []
# words = [t for w in remove_one_two for t in w if t.lower() not in stopwords]
for t in remove_one_two:
    for w in t:
        if w.lower() not in stopwords:
            words.append(w)
            if w not in vocab :
                vocab[w] = 0
            vocab[w] +=1
print(words)
print("위에까지가 단어였음")
# print(t) 리스트 하나가 출력


# 문장의 길이를 히스토그램으로 나타내는건데 그림이 안그려져서 일단 넘어갔습니다.
# plt.hist([len(sent) for sent in words],bins=50)
# plt.show()


print("문장의 최대길이: " , max(len(l) for l in words)) # 문장의 최대길이:  148
print("문장의 평균길이: ", sum(map(len,words))/len(words)) # 문장의 평균길이:  5.715457960436906


# 무장의 길이 중 주어진 숫자 이하의 샘플의 비율을 보여주는 함수입니다.
# def below_len(max_len, sentences):
#     cnt = 0
#     for sent in sentences :
#         if len(sent) <= max_len:
#             cnt += 1
#     print(f' 전체 문장 중 길이가 {max_len} 이하의 샘플의 비율:{(cnt/len(sentences))*100}')
#
# for x in range(0,20,1):
#      below_len(x, words)

# 패딩을 하기 위해 pad_sequences를 사용했는데 패딩이 제대로 되지 않고 출력해보면 () 빈 값만 잔뜩 출력되는 문제가 생깁니다.. 이걸 해결하고 나면 모델을 훈련시킬 때 사용할 수 있습니다.
# 패딩이 이상하게 됨
# tokenizer = Tokenizer()
# tokenizer.fit_on_texts(words)
# word_index = tokenizer.word_index
#
# encoded = tokenizer.texts_to_sequences(words)
# print(encoded)
# padded = pad_sequences(encoded)
# print(padded)


tokenizer = Tokenizer()
tokenizer.fit_on_texts(remove_one_two)
encoded = tokenizer.texts_to_sequences(remove_one_two)
max_len = 6

for item in encoded: # 각 문장에 대해서
    while len(item) < max_len:   # max_len보다 작으면
        item.append(0)

padded_np = np.array(encoded)

print(padded_np)