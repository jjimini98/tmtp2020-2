#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import nltk
import matplotlib.pyplot as plt
from nltk import word_tokenize
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
import tensorflow as tf
import numpy as np
from sklearn.model_selection import StratifiedShuffleSplit
from tensorflow.keras.utils import to_categorical


# In[2]:


data = pd.read_csv("hotel.csv")

#긍정과 부정 리뷰를 띄어쓰기로 합친 새로운 열 total_review 생성
data['total_review'] = data['Negative_Review']+" "+data['Positive_Review']

#필요없는 열 모두 제거
del data['Unnamed: 0']
del data['Negative_Review']
del data['Positive_Review']

# No negative, No positive, Nothing 은 빈칸으로 replace
for x in data['total_review']:
    if 'No Negative' in x:
       data['total_review']= data['total_review'].str.replace("No Negative","-")
    
    if 'No Positive' in x:
        
        data['total_review']= data['total_review'].str.replace("No Positive","-")

    if 'Nothing' in x:
        data['total_review'] = data['total_review'].str.replace('Nothing',"-")

# 열 순서 변경
data = data[['total_review','Reviewer_Score']]


# In[3]:


data.isnull().sum() #null 수 확인


# In[4]:


data.isna().sum() #na 수 확인


# In[5]:


len(data['total_review']) 
len(data['Reviewer_Score'])


# In[6]:


data['total_review'].value_counts() #value 값 별로 count 확인


# In[7]:


data['Reviewer_Score'].value_counts()


# In[8]:


#df라는 데이터에 변수명 review, label로 변경
df = data.copy()
df.rename(columns = {'total_review':'review'},inplace=True)
df.rename(columns = {'Reviewer_Score':'label'},inplace=True)
df 


# In[9]:


print(df.dtypes) #자료형 확인


# In[10]:


#데이터 나누기
splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=100)


# In[11]:


for train_index, test_index in splitter.split(df, df['label']):
    train_data = df.loc[train_index]
    test_data = df.loc[test_index]


# In[12]:


train_data.index = pd.RangeIndex(len(train_data.index))
test_data.index = pd.RangeIndex(len(test_data.index))


# In[13]:


print(f"train data개수 : {len(train_data)}")
display(train_data.head(10))
print(f"test data개수 : {len(test_data)}")
display(test_data.head(10))


# In[14]:


# one hot encoding
y_train = train_data[["label"]]
y_test = test_data[["label"]]

display(y_train.head(10))
display(y_test.head(10))


# In[15]:


y_train = to_categorical(y_train,len(df['label'].unique()))
y_test = to_categorical(y_test,len(df['label'].unique()))


# In[16]:


print(y_train.shape) #행 열 수를 반환
print(y_train.size) #전체 원소의 갯수 반환
print(len(y_train)) #차원의 행 수를 반환


# In[17]:


print(y_test.shape) #행 열 수를 반환
print(y_test.size) #전체 원소의 갯수 반환
print(len(y_test)) #차원의 행 수를 반환


# In[18]:


print(y_train[2]) #원핫벡터 결과 확인
print(y_train[66883])
print(y_train[424])
print(y_train[11111])
print(y_train[6])
print(y_train[7875])
print(y_train[222])
print(y_train[75413])
print(y_train[51387])


# In[19]:


# 문장분리
X_train_comment = [] #361016
for review in train_data['review']:
       X_train_comment.append(review)
       
X_test_comment = [] #154722
for review in test_data['review']:
       X_test_comment.append(review)


# In[20]:


#문장분리 확인
X_train_comment


# In[21]:


X_train_comment[0]


# In[22]:


X_test_comment


# In[23]:


#토큰화 X_train
X_train_token = [] #token length : 361016 리스트 안에 리스트형태
for i in range(0, len(X_train_comment)):
    text = word_tokenize(X_train_comment[i])
    X_train_token.append(text)


# In[24]:


#토큰화 X_test
X_test_token = [] #token length : 154722 리스트 안에 리스트형태
for i in range(0, len(X_test_comment)):
    text = word_tokenize(X_test_comment[i])
    X_test_token.append(text)


# In[25]:


X_train_token


# In[26]:


X_test_token


# In[27]:


words_skipped = [e for e in X_train_token if len(e) < 3] #길이가 짧았던 단어 확인
print(words_skipped[:20])


# In[28]:


#블용어 목록 얻음 확인
stopwords = nltk.corpus.stopwords.words('english')
print(stopwords[:20])


# In[29]:


#불용어 제거(X_train)
#remove_one_two = [e for e in X_train if len(e)>=3] 
stopwords = nltk.corpus.stopwords.words('english')
X_train = []

for t in range(len(X_train_token)):
    line = []
    for w in range(len(X_train_token[t])):
        if X_train_token[t][w].lower() not in stopwords:
            line.append(X_train_token[t][w])
    X_train.append(line)


# In[30]:


#불용어 제거 비교(X_train_token, X_train)
X_train_token


# In[31]:


X_train


# In[32]:


print(len(X_train_token))
print(len(X_train))


# In[33]:


#불용어 제거(X_test)
stopwords = nltk.corpus.stopwords.words('english')
X_test = []

for t in range(len(X_test_token)):
    line = []
    for w in range(len(X_test_token[t])):
        if X_test_token[t][w].lower() not in stopwords:
            line.append(X_test_token[t][w])
    X_test.append(line)


# In[34]:


#불용어 제거 비교(X_test_token, X_test)
X_test_token


# In[35]:


X_test


# In[36]:


print(len(X_test_token))
print(len(X_test))


# In[37]:


#등장 빈도 확인
def check_frequency(num,tokenizer):
    threshold = num
    total_cnt = len(tokenizer.word_index)
    rare_cnt = 0
    total_freq = 0
    rare_freq = 0
    
    for word, freq in tokenizer.word_counts.items():
        total_freq += freq
        if total_freq < threshold:
            rare_cnt += 1
            rare_freq += freq
            
    print(f'최소 등장 빈도 : {threshold-1}')
    print(f'어휘 집합 크기 : {total_cnt}')
    print(f'희귀 단어 수 : {rare_cnt}')
    print(f'어휘 집합에서 희귀 단어의 비중 : {(rare_cnt/total_cnt)*100}')
    print(f'전체 단어 빈도에서 희귀 단어 빈도 : {(rare_freq/total_freq)*100}')
    
    return total_cnt, rare_cnt


# In[38]:


test_tokenizer = Tokenizer()
test_tokenizer.fit_on_texts(X_train)


# In[41]:


threshold = len(df['label'].unique())
total_cnt,rare_cnt = check_frequency(threshold, test_tokenizer)
vocab_size = total_cnt - rare_cnt+1


# In[42]:


myTokenizer = Tokenizer(vocab_size)
myTokenizer.fit_on_texts(X_train)
print(myTokenizer.index_word)


# In[43]:


X_train = myTokenizer.texts_to_sequences(X_train)
X_test = myTokenizer.texts_to_sequences(X_test)


# In[44]:


plt.hist([len(sent) for sent in X_train], bins=50)
plt.show()


# In[45]:


print("문장의 최대길이: " , max(len(l) for l in X_train)) # 문장의 최대길이:  381
print("문장의 평균길이: ", sum(map(len,X_train))/len(X_train)) # 문장의 평균길이:  18.819764220976356


# In[46]:


#문장의 길이 중 주어진 숫자 이하의 샘플의 비율을 보여주는 함수입니다.
def below_len(max_len, sentences):
    cnt = 0
    for sent in sentences :
        if len(sent) <= max_len:
            cnt += 1
    print(f' 전체 문장 중 길이가 {max_len} 이하의 샘플의 비율:{(cnt/len(sentences))*100}')

for x in range(0,200,10):
    below_len(x, X_train)


# In[47]:


max_len = 70

X_train = pad_sequences(X_train, maxlen=max_len)
X_test = pad_sequences(X_test, maxlen=max_len)


# In[48]:


print(X_train)
print(X_train[22])
print(X_test)
print(X_test[56])


# In[49]:


print(X_train.shape)
print(y_train.shape)
print(X_test.shape)
print(y_test.shape)


# In[ ]:




