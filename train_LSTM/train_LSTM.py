import pandas as pd
import nltk
import matplotlib.pyplot as plt
from nltk import word_tokenize
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
import tensorflow as tf
import numpy as np
from sklearn.model_selection import StratifiedShuffleSplit
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential , Model, load_model
from tensorflow.keras.layers import Dense, Embedding, LSTM , Bidirectional, Dropout
from tensorflow.keras.callbacks import EarlyStopping , ModelCheckpoint
import sys , os
from gensim.models import word2vec , KeyedVectors

data = pd.read_csv("hotel.csv")

#긍정과 부정 리뷰를 띄어쓰기로 합친 새로운 열 total_review 생성
data['total_review'] = data['Negative_Review']+" "+data['Positive_Review']

#필요없는 열 모두 제거
del data['Unnamed: 0']
del data['Negative_Review']
del data['Positive_Review']

# No negative, No positive, Nothing 은 빈칸으로 replace
for x in data['total_review']:
    if 'No Negative' in x:
       data['total_review']= data['total_review'].str.replace("No Negative","-")
    
    if 'No Positive' in x:
        
        data['total_review']= data['total_review'].str.replace("No Positive","-")

    if 'Nothing' in x:
        data['total_review'] = data['total_review'].str.replace('Nothing',"-")

# 열 순서 변경
data = data[['total_review','Reviewer_Score']]


#df라는 데이터에 변수명 review, label로 변경
df = data.copy()
df.rename(columns = {'total_review':'review'},inplace=True)
df.rename(columns = {'Reviewer_Score':'label'},inplace=True)


#데이터 나누기
splitter = StratifiedShuffleSplit(n_splits=1, test_size=0.3, random_state=100)




for train_index, test_index in splitter.split(df, df['label']):
    train_data = df.loc[train_index]
    test_data = df.loc[test_index]




train_data.index = pd.RangeIndex(len(train_data.index))
test_data.index = pd.RangeIndex(len(test_data.index))





# one hot encoding
y_train = train_data[["label"]]
y_test = test_data[["label"]]



y_train = to_categorical(y_train,len(df['label'].unique()))
y_test = to_categorical(y_test,len(df['label'].unique()))


# 문장분리
X_train_comment = [] #361016
for review in train_data['review']:
       X_train_comment.append(review)
       
X_test_comment = [] #154722
for review in test_data['review']:
       X_test_comment.append(review)


#토큰화 X_train
X_train_token = [] #token length : 361016 리스트 안에 리스트형태
for i in range(0, len(X_train_comment)):
    text = word_tokenize(X_train_comment[i])
    X_train_token.append(text)




#토큰화 X_test
X_test_token = [] #token length : 154722 리스트 안에 리스트형태
for i in range(0, len(X_test_comment)):
    text = word_tokenize(X_test_comment[i])
    X_test_token.append(text)





words_skipped = [e for e in X_train_token if len(e) < 3] #길이가 짧았던 단어 확인
# print(words_skipped[:20])



#블용어 목록 얻음 확인
stopwords = nltk.corpus.stopwords.words('english')
# print(stopwords[:20])



#불용어 제거(X_train)
#remove_one_two = [e for e in X_train if len(e)>=3] 
stopwords = nltk.corpus.stopwords.words('english')
X_train = []

for t in range(len(X_train_token)):
    line = []
    for w in range(len(X_train_token[t])):
        if X_train_token[t][w].lower() not in stopwords:
            line.append(X_train_token[t][w])
    X_train.append(line)



#불용어 제거(X_test)
stopwords = nltk.corpus.stopwords.words('english')
X_test = []

for t in range(len(X_test_token)):
    line = []
    for w in range(len(X_test_token[t])):
        if X_test_token[t][w].lower() not in stopwords:
            line.append(X_test_token[t][w])
    X_test.append(line)



#등장 빈도 확인
def check_frequency(num,tokenizer):
    threshold = num
    total_cnt = len(tokenizer.word_index)
    rare_cnt = 0
    total_freq = 0
    rare_freq = 0
    
    for word, freq in tokenizer.word_counts.items():
        total_freq += freq
        if total_freq < threshold:
            rare_cnt += 1
            rare_freq += freq
            
    print(f'최소 등장 빈도 : {threshold-1}')
    print(f'어휘 집합 크기 : {total_cnt}')
    print(f'희귀 단어 수 : {rare_cnt}')
    print(f'어휘 집합에서 희귀 단어의 비중 : {(rare_cnt/total_cnt)*100}')
    print(f'전체 단어 빈도에서 희귀 단어 빈도 : {(rare_freq/total_freq)*100}')
    
    return total_cnt, rare_cnt


test_tokenizer = Tokenizer()
test_tokenizer.fit_on_texts(X_train)



threshold = len(df['label'].unique())
total_cnt,rare_cnt = check_frequency(threshold, test_tokenizer)
vocab_size = total_cnt - rare_cnt+1


myTokenizer = Tokenizer(vocab_size)
myTokenizer.fit_on_texts(X_train)
# print(myTokenizer.index_word)


X_train = myTokenizer.texts_to_sequences(X_train)
X_test = myTokenizer.texts_to_sequences(X_test)



#문장의 길이 중 주어진 숫자 이하의 샘플의 비율을 보여주는 함수입니다.
def below_len(max_len, sentences):
    cnt = 0
    for sent in sentences :
        if len(sent) <= max_len:
            cnt += 1
    # print(f' 전체 문장 중 길이가 {max_len} 이하의 샘플의 비율:{(cnt/len(sentences))*100}')

for x in range(0,200,10):
    below_len(x, X_train)



max_len = 70
X_train = pad_sequences(X_train, maxlen=max_len)
X_test = pad_sequences(X_test, maxlen=max_len)




#양방향 LSTM 만들기

# model = Sequential()
# model.add(Embedding(vocab_size,100))
# model.add(Bidirectional(LSTM(128,recurrent_dropout=0.2)))
# model.add(Dense(100,activation = 'softmax'))
# model.add(Dense(37,activation='softmax'))
#
# model.compile (optimizer = 'Adam',
#                loss = 'categorical_crossentropy',
#                metrics = ['accuracy'])
#
# model_dir = f'{"./model_dir"}/bidirectional_LSTMmodel'
# if not os.path.exists(model_dir):
#     os.mkdir(model_dir)
# model_path = model_dir + "./LSTM_bidirectional.model"
#
# ck = ModelCheckpoint(filepath=model_path, monitor= 'val_loss', verbose = 1, save_best_only=True)
# es = EarlyStopping(monitor = 'val_loss' , patience = 3)
#
# # 에포크 20으로 했다가 숨넘어갈거 같아서 5로 낮춰서 진행해봤습니다.
# history = model.fit(X_train, y_train,
#                     batch_size = 361016,
#                     epochs = 10,
#                     callbacks = [ck,es]
#                     validation_set = )



from tensorflow.keras.models import load_model
model = load_model('./model_dir/bidirectional_LSTMmodel/LSTM_bidirecional.model')

y_pred = model.predict(X_test, batch_size = 50)
x_test_idx= np.random.choice(x_test.shape[0], 5)
for i in range(10):
    print('True : ' + str(argmax(y_test[x_test_idx[i]])) + ', Predict : ' + str(y_pred[i]))
